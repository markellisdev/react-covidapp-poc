import apiClient from './client';

const endpoint = '/categories';

const getCategories = () => (endpoint);

export default {
    getCategories,
}
