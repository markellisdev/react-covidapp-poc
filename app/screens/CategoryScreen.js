import React, { useEffect, useState } from 'react';
import { ImageBackground, StyleSheet, View, SafeAreaView, Button, Text, FlatList, Image} from 'react-native';
import Constants from 'expo-constants';
import Header from '../components/Header';
import categoriesApi from '../api/categories';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import NextButton from '../components/NextButton';
import BackButton from '../components/BackButton';

const CategoryScreen = ({route, navigation}) => {

    // simplified json data to make logs shorter while developing. REPLACE with heyirys_covid.json when working
    const apiReturn = require('../API-simulated/heyirys_covid.json')


    // This returns an array - this could map
    const result = Object.keys(apiReturn.categories).map(key => ({[key]: apiReturn.categories[key]}));
    // console.log('These are the object values of result: ', Object.values(apiReturn.categories))
    const catValues = Object.values(apiReturn.categories)

    // cats is an Object, not array
    const cats = JSON.parse(JSON.stringify(apiReturn))

    // Get list of category keys to try flatlist
    const categoryKeysList = Object.keys(cats["categories"])

    const catNamesEn = []
    const catNamesEs = []

    let subcatNamesEn = [1, 2]
    let subcatNamesEs = []

    // ---- Testing iteration to retrieve the proper values
    for (var i = 0; i < categoryKeysList.length; i++) {
        // ---- Get En & Es names for categories in array form
        catNamesEn.push(apiReturn["categories"][categoryKeysList[i]]["name"]["en"])
        catNamesEs.push(apiReturn["categories"][categoryKeysList[i]]["name"]["es"])
        // If subcat, get names
        if (apiReturn["categories"][categoryKeysList[i]]["subcategories"]) {
            subcatNamesEn.push(apiReturn["categories"][categoryKeysList[i]]["subcategories"]["en"])
            subcatNamesEs.push(apiReturn["categories"][categoryKeysList[i]]["subcategories"]["es"])
        }
    }


    return (
        <ImageBackground
            style={styles.background, styles.screen}
        >
            <View style={styles.textContainer}>
                <Text style={styles.h1}>Category</Text>
                <Text style={{marginBottom:40}}>Select an option to continue</Text>
            </View>

            <View style={styles.headerColumn}>
                <FlatList
                    data={catValues}
                    keyExtractor={category => category.order.toString()}
                    renderItem={({ item }) => {
                        const title= item.name.en
                        return (
                        <>
                        <Header
                            imageUrl={item.icon}
                            title={title}
                            onPress={() => navigation.navigate("SubCategory", {
                                subcatsEn: subcatNamesEn,
                                subcatsEs: subcatNamesEs,
                                catTitle: title})}/>
                        {/* hr line solution found here: https://stackoverflow.com/a/61611734 */}
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                            <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                        </View>
                        </>)
                    }
                }
                />

            </View>
            {/* Button Row with Back and Next Buttons */}
            <View style={styles.buttonRow}>
                <BackButton
                    onPress={() => navigation.pop()}
                    title="Back"
                />
                <NextButton
                    // onPress={() => props.navigation.goBack()}
                    title="Go back home"
                    // style={styles.buttonStyle}
                />
            </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-start',
        // paddingTop: 50,
        backgroundColor: '#F6F2FF'
    },
    h1: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 10,
    },
    textContainer: {
        paddingLeft: 20,
    },
    headerColumn: {
        marginBottom: 140,
    },
    screen: {
        paddingTop: Constants.statusBarHeight,
    }
})

export default CategoryScreen;