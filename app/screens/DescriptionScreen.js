import React, {useState} from 'react';
import { ImageBackground, StyleSheet, View, SafeAreaView, Button, Text, TextInput} from 'react-native';
import Header from '../components/Header';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import NextButton from '../components/NextButton';
import BackButton from '../components/BackButton';

const DescriptionScreen = (props) => {
    const [description, setDescription] = useState('');


    return (
        <ImageBackground
            style={styles.background}
        >
            <View style={styles.textContainer}>
                <Text style={styles.h1}>Report Price Gouging</Text>
                <Text style={{marginBottom:40}}>Please provide a Brief Description of the issue.</Text>
            </View>
            <View style={styles.textInputContainer}>
                {/* test to see if getting input */}
                {/* <Text>{description}</Text> */}
                <TextInput
                    onChangeText={text => setDescription(text)}
                    placeholder="Type here, 200 characters or 29 words allowed."
                    style={{padding:10, color: 'rgb(32, 32, 73)'}}
                />
            </View>
            <View style={styles.buttonRow}>
                <BackButton
                    onPress={() => props.navigation.goBack()}
                    title="Back"
                />
                <NextButton
                    // onPress={() => props.navigation.goBack()}
                    title="Go back home"
                    // style={styles.buttonStyle}
                />
            </View>
        </ImageBackground>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-start',
        paddingTop: 50,
        backgroundColor: '#F6F2FF'
    },
    h1: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 10,
    },
    textContainer: {
        paddingLeft: 20,
    },
    textInputContainer: {
        borderRadius: 10,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: 'black',
        minHeight: 200,
        margin: 20,
        marginBottom: 140,
        backgroundColor: 'rgb(232, 218, 255)'
    },
})

export default DescriptionScreen;