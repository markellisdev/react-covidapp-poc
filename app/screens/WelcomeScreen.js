import React from 'react';
import { ImageBackground, StyleSheet, View, SafeAreaView, Button} from 'react-native';
import Header from '../components/Header';
import Constants from 'expo-constants';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import CategoryScreen from './CategoryScreen';
import { TouchableHighlight } from 'react-native-gesture-handler';

const WelcomeScreen = (props) => {
    return (
        <SafeAreaView
            style={styles.background, styles.screen}
            // source={require('../assets/chair.jpg')}
        >
            <View>
                <Header />
            </View>
            <TouchableHighlight
                underlayColor='aqua'
                onPress={() => props.navigation.navigate('Category')}
            >
                <Button
                    title="Go to categories"
                />
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor='aqua'
                onPress={() => props.navigation.navigate('Description')}
            >
                <Button
                    title="Go to description"
                />
            </TouchableHighlight>
            <TouchableHighlight
                underlayColor='aqua'
                onPress={() => props.navigation.navigate('Map')}
            >
                <Button
                    title="Go to maps"
                />
            </TouchableHighlight>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-start',
        // paddingTop: 50,
        // backgroundColor: 'green' my son's choice while helping
        backgroundColor: "#F6F2FF"
    },
    screen: {
        paddingTop: Constants.statusBarHeight,
    },
})

export default WelcomeScreen;