import React, { Component } from 'react';
import { ImageBackground, StyleSheet, View, SafeAreaView, Button, Text} from 'react-native';
import Header from '../components/Header';

import NextButton from '../components/NextButton';
import BackButton from '../components/BackButton';

export default class SubCategoryScreen extends Component {
    // Pull in "data" as props --- not working
    constructor(props){
        super(props)
    }
    
    render() {

        const apiReturn = require('../API-simulated/sample.json')
        
        return (
            <ImageBackground
                style={styles.background}
            >
                <View style={styles.textContainer}>
                    <Text style={styles.h1}>Category: {this.props.route.params.catTitle}</Text>
                    <Text style={{marginBottom:40}}>Please select the type of product or service that is being sold at an exorbitant or excessive price.</Text>
                </View>
                {/* hr line solution found here: https://stackoverflow.com/a/61611734 */}
                <View style={styles.headerColumn}>
                    <Header title={apiReturn["categories"]["priceGouging"]["subcategories"]["buildingMaterials"]["name"]["en"]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header
                        title={apiReturn["categories"]["priceGouging"]["subcategories"]["constructionTools"]["name"]["en"]}
                        onPress={() => this.props.navigation.navigate("SubCategory", this.title)}
                    />
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header title={apiReturn["categories"]["priceGouging"]["subcategories"]["food"]["name"]["en"]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header title={apiReturn["categories"]["priceGouging"]["subcategories"]["fuel"]["name"]["en"]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header
                        title={apiReturn["categories"]["priceGouging"]["subcategories"]["lodging"]["name"]["en"]}
                        onPress={() => navigation.navigate("SubCategory", title)}
                    />
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header title={apiReturn["categories"]["priceGouging"]["subcategories"]["medicine"]["name"]["en"]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                    <Header title={apiReturn["categories"]["priceGouging"]["subcategories"]["other"]["name"]["en"]}/>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent:'center'}}>
                        <View style={{height: 1, backgroundColor: 'black', width:'90%'}} />
                    </View>
                </View>
                <View style={styles.buttonRow}>
                <BackButton
                    onPress={() => this.props.navigation.pop()}
                    title="Back"
                />
                <NextButton
                    // onPress={() => props.navigation.goBack()}
                    title="Go back home"
                    // style={styles.buttonStyle}
                />
                </View>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'flex-start',
        paddingTop: 50,
        backgroundColor: '#F6F2FF'
    },
    h1: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 10,
    },
    buttonRow: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        paddingVertical: 10,
    },
    textContainer: {
        paddingLeft: 20,
    },
    headerColumn: {
        marginBottom: 40,
    },
})