import React, { useState, useEffect } from 'react';
import { StyleSheet, View } from 'react-native';
import Header from '../components/Header';
import MapView, {Marker, Callout} from 'react-native-maps';
import Permissions from 'expo';
import { TextInput } from 'react-native-gesture-handler';
import * as Location from 'expo-location';

export default class MapScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            latitude: 29.42934083341083,
            longitude: -98.48763303426837,
            error: null,
        }
    }
    
                // this is not yet working because can't access property message, error is undefined    
    componentDidMount() {
        navigator.geolocation.getCurrentPosition(
            position => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    error: null
                },
                // error => this.setState({ error: error.message }),
                // // error => console.log('Error:', error), shows Error: undefined
                // { enableHighAccuracy: true, timeout: 20000, maximumAge: 2000}
            );
        })
    }

    render() {
        return(
            <MapView
                style={StyleSheet.absoluteFillObject}
                provider={MapView.PROVIDER_GOOGLE}
                showsUserLocation={true}
                // Set San Antonio as initialRegion
                initialRegion={{
                    // latitude: this.state.latitude,
                    // longitude: this.state.longitude,
                    latitude: 29.42934083341083,
                    longitude: -98.48763303426837,
                    latitudeDelta: 0.922,
                    longitudeDelta: 0.421,
                }}>
                    <Marker
                        // coordinate={{ latitude: 29.42934083341083, longitude: -98.48763303426837}}
                        coordinate={this.state} 
                        // this seems to always show SFO.... only on sim.... actually working on my iPhone`
                        pinColor='grey'
                        title= "Your location"
                    />
                    <View style={{ position: 'absolute', top: 10, width: '100%' }}>
                        <TextInput
                        style={{
                            borderRadius: 10,
                            margin: 10,
                            color: '#000',
                            borderColor: '#666',
                            backgroundColor: '#FFF',
                            borderWidth: 1,
                            height: 45,
                            paddingHorizontal: 10,
                            fontSize: 18,
                            justifyContent: 'center',
                        }}
                        placeholder={'Type your location'}
                        placeholderTextColor={'#666'}
                        />
                    </View>
                </MapView>
                );
            }
            
}
    const styles = StyleSheet.create({
        background: {
            flex: 1,
            justifyContent: 'flex-start',
            paddingTop: 50,
            backgroundColor: 'green'
        },
    });