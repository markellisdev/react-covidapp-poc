import React, { Component } from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import CategoryScreen from '../screens/CategoryScreen';
import DescriptionScreen from '../screens/DescriptionScreen';
import MapScreen from '../screens/MapScreen';
import SubCategoryScreen from '../screens/SubCategoryScreen';
import WelcomeScreen from '../screens/WelcomeScreen';

const Stack = createStackNavigator();

export default class AppNavigator extends Component {

    render() {
        const apiReturn = require('../API-simulated/heyirys_covid.json')

        return (
            <Stack.Navigator
                screenOptions={{
                headerStyle: { backgroundColor: '#F6F2FF' },
                headerTintColor: "#000",
            }}>

                <Stack.Screen name="Hey Irys" component={WelcomeScreen} />
                <Stack.Screen name="Category" component={CategoryScreen} data={apiReturn}/>
                <Stack.Screen name="Description" component={DescriptionScreen} />
                <Stack.Screen name="Map" component={MapScreen} />
                <Stack.Screen name="SubCategory" component={SubCategoryScreen} data={apiReturn}/>
            </Stack.Navigator>
        )
    }
}
