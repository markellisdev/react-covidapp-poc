import React from 'react';
import {createStackNavigator} from '@react-navigation/stack'
import SubCategoryScreen from '../screens/SubCategoryScreen';
import DescriptionScreen from '../screens/DescriptionScreen';

const Stack = createStackNavigator();

const SubCategoryNavigator = () => (
    <Stack.Navigator>
        <Stack.Screen name="SubCategory" component={SubCategoryScreen} />
        <Stack.Screen name="Description" component={DescriptionScreen} />
    </Stack.Navigator>
);

export default SubCategoryNavigator;