import { DefaultTheme } from '@react-navigation/native';

export default {
    ...DefaultTheme,
    colors: {
        ...DefaultTheme.colors,
        primary: '#e1def9',
        background: '#e1def9',
    }
};