import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

function NextButton(props) {
    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>Next</Text>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: 'red',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        width: 160,
    },
    buttonText: {
        color: '#F6F2FF'
    }
})

export default NextButton;