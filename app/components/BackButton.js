import React from 'react';
import { View, StyleSheet, Text } from 'react-native';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';

function BackButton(props) {
    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={styles.button}>
                <Text style={styles.buttonText}>Back</Text>
            </View>
        </TouchableWithoutFeedback>
    );
}

const styles = StyleSheet.create({
    button: {
        backgroundColor: '#F6F2FF',
        borderColor: 'red',
        borderWidth: 1,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        padding: 15,
        width: 160,
    },
    buttonText: {
        color: 'red',
    }
})

export default BackButton;