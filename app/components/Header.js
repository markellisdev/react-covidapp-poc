import React from 'react';
import { ImageBackground, StyleSheet, View, Text, TouchableWithoutFeedback, Image} from 'react-native';

const Header = (props) => {
    return (
        <TouchableWithoutFeedback onPress={props.onPress} style={styles.header}>
            <View
                style={styles.header}
            >
                {props.imageUrl && <Image style={{height:36, width:36}} source={{uri: props.imageUrl}}/>}
                <Text style={styles.headerText} >{props.title}</Text>
            </View>
        </TouchableWithoutFeedback>
    );
}

Header.defaultProps = {
    title: 'Hey Irys',
};

const styles = StyleSheet.create({
    header: {
        width: '100%',
        height: 70,
        padding: 15,
        // color for HeyIrys #e1def9
        backgroundColor: 'rgb(232, 218, 255)',
        flexDirection: 'row',
        alignItems: 'center'
    },
    headerText: {
        color: 'black',
        fontSize: 16,
        paddingLeft: 10,
    }
})

export default Header;