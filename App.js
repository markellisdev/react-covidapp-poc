import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { Image, StyleSheet, Text, TouchableOpacity, View, SafeAreaView} from 'react-native';
import 'react-native-gesture-handler';
// Navigation
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
// Maps
import MapView from 'react-native-maps';
import logo from './app/assets/logo.png';

// Navigation Imported
import AppNavigator from './app/navigation/AppNavigator';
import navigationTheme from './app/navigation/navigationTheme';


const Drawer = createDrawerNavigator();

export default function App() {
  const [items, setItems] = useState([]);
  // return <WelcomeScreen />
  return (
    <NavigationContainer theme={navigationTheme}>
      <AppNavigator />
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: { width:305, height:159, marginBottom: 20 },
  instructions: {color:'#888', fontSize: 18, marginHorizontal: 15, marginBottom: 10},
  button: { backgroundColor: 'blue', padding: 20, borderRadius: 5 },
  buttonText: { fontSize: 20, color: '#fff' },
});


// -------------------- COMMENT ALL ABOVE BACK IN ------------------------
// ------ Following this to better understand state & props, https://www.opencodez.com/react-native/react-native-props-and-state.htm
// import React, { Fragment } from 'react';
// import Category from './app/components/Category';
// import LanguageSelector from './app/components/LanguageSelector';

// const App = () => {
//   const apiReturn = require('./app/API-simulated/sample.json')



//   return (
//     <Fragment>
//       <LanguageSelector data={apiReturn.categories}/>
//     </Fragment>
//   );
// };
// export default App;