# Dashboard built with React Native
This is a project built to develop and demonstrate proficiencies in React Native

## Install

This app is created using [Expo](https://github.com/expo/expo)

### Expo
You'll need to install the Expo CLI to get things going. You can find that information in the above link or if you're already familiar with React and JavaScript, you can
`npm install --global expo-cli`

Once you have the repo locally, you can run the app with `expo start`
Then you'll choose where you'd like to see it, from this prompt
```To run the app, choose one of:
 › Scan the QR code above with the Expo app (Android) or the Camera app (iOS).
 › Press a for Android emulator, or i for iOS simulator, or w to run on web.
 › Press e to send a link to your phone with email.
 ```
NOTE: to have the map show the pin of your location, you'll need to choose the first option, using the Expo mobile app on device of your choice. Otherwise, your location will default to San Francisco, where the app is hosted.

## Current Views
Current as of Friday, February 26, 2021

![Initial Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/InitialScreen-SimulatorScreenShot-iPhoneXR.png)
![Category Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/Category-SimulatorScreenShot-iPhoneXR.png)
![Subcategory Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/Subcategory-SimulatorScreenShot-iPhoneXR.png)
![Description Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/Description-SimulatorScreenShot-iPhoneXR.png)
![Map Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/Map-SimulatorScreenShot-iPhoneXR.png)
![Map Location Pin Screen](https://bitbucket.org/markellisdev/react-covidapp-poc/raw/main/Images-README/Map-LocationPin-SimulatorScreenShot-iPhoneXR.png)

## Resources
[ReactNative Docs](https://reactnative.dev/docs/environment-setup)

[Expo Docs](https://docs.expo.io/tutorial/image-picker/)

[Adding Google Maps](https://dev.to/kpete2017/how-to-add-google-maps-to-your-expo-react-native-project-2fe5)